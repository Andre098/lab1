import socket
import tqdm
import os
import random
import string
import hashlib

def generate_random_letters(filename,size,checksum):
    chars = ''.join([random.choice(string.printable) for i in range(size)]) #1
    with open(filename, 'w') as f:
        checksum.update(chars.encode())
        f.write(chars)
    return checksum.hexdigest()

SEPARATOR = "<SEPARATOR>"
BUFFER_SIZE = 4096
host = "0.0.0.0"
port = 5001
filename = "text.txt"
md5_check = hashlib.md5()
check = generate_random_letters(filename,20,md5_check)
filesize = os.path.getsize(filename)
s = socket.socket()
s.bind((host, port))
s.listen(5)
print(f"[*] Listening as {host}:{port}")
conn, addr = s.accept()
print("[*] Received connection");
conn.sendall(f"{filename}{SEPARATOR}{filesize}{SEPARATOR}{check}".encode())
progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
with open(filename, "rb") as f:
    while True:
        bytes_read = f.read(BUFFER_SIZE)
        if not bytes_read:
            # file transmitting is done
            break
        # we use sendall to assure transimission in 
        # busy networks
        conn.sendall(bytes_read)
        # update the progress bar
        progress.update(len(bytes_read))
# close the socket
conn.close()
s.close()