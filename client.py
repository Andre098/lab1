import socket
import tqdm
import os
import hashlib

SERVER_HOST = "172.21.0.2"
SERVER_PORT = 5001
BUFFER_SIZE = 4096
SEPARATOR = "<SEPARATOR>"
md5_check = hashlib.md5()
s = socket.socket()
s.connect((SERVER_HOST, SERVER_PORT))
print(f"[*] Connected to {SERVER_HOST}:{SERVER_PORT}")
received = s.recv(BUFFER_SIZE).decode()
filename, filesize, checksum = received.split(SEPARATOR)
filename = os.path.basename(filename)
filesize = int(filesize)
progress = tqdm.tqdm(range(filesize), f"Receiving {filename}", unit="B", unit_scale=True, unit_divisor=1024)
with open(filename, "wb") as f:
    while True:
        bytes_read = s.recv(BUFFER_SIZE)
        if not bytes_read:
            if md5_check.hexdigest() != checksum:
                print("File is not full")    
            break
        md5_check.update(bytes_read)
        f.write(bytes_read)
        progress.update(len(bytes_read))

s.close()